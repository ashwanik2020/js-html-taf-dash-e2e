import {Component} from "../../../dash-e2e-pageobjects/aspect-objects/Component";
let random = require("random-js")();

import {Navigate} from "../../../dash-e2e-pageobjects/aspect-objects/Navigate";
import {DashDashboardProfilePage} from "../../../dash-e2e-pageobjects/page-objects/DashDashboardProfilePage";


describe('[DASH: CREATE FLOWS]', function() {

    let dash_value :any;

    it('TEST[001]: Create a new dashboard with basic configurations and verify it', function() {

        Navigate.visit('DashDashboardPage').
            check_And_Validate_dash_page_title('Dashboards').
            step_click_on_the_new_team_dashboard_button().
            step_create_basic_dashboard('Dashboard - '+dash_value,'Auto Desc','R&D','VP','QA').
            check_and_validate_dashboard_profile_title('Dashboard - '+dash_value).
            step_move_to_landing_page_by_click_header_dashboard_tab().
            step_enter_dash_record_under_search_box('Dashboard - '+dash_value).
            check_and_validate_dash_record_under_table('Dashboard - '+dash_value);
    });

    it('TEST[002]: Create app web-link type web-viewer widgets under basic dash-board - general link site', function() {

        Component.CreateBasicDashboard(dash_value);
            new DashDashboardProfilePage().
            step_click_on_profile_edit_setting().
            step_adding_given_dashapp_widget_using_add_app('Web Viewer').
            check_and_validate_recently_added_new_widget_title('Web Viewer').
            step_press_webviewer_adding_icon().
            step_webviewer_set_site_details_and_test('http://www.ideaboardz.com/').
            step_webviewer_set_site_bookmark_and_add_as_link_and_save_widget('Idea Boardz').
            check_and_validate_newly_created_site_under_webviewer_panel('Idea Boardz','http://www.ideaboardz.com/').
            check_and_validate_created_webviewer_site_in_new_tab('link','http://www.ideaboardz.com/');
    });

    beforeEach(() => {
        dash_value = random.integer(100000, 999999);
        console.log('');
        console.log('');
        console.log('');
        console.log('************************************************************************************************ ');
    });

    afterAll(() => {
        console.log('******************************** [DASH Test Automation: Suite] - COMPLETED ! ************************************');
    });


});

