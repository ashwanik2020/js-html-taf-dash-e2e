export default
{
    "DashHeaderModule": {
        "lnkDashLogo": {findBy: "css", value: ".nav-logo"},
        "lnkDashHome": {findBy: "css", value: ".nav-dashboards"},
        "lnkDashNotification": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[2]"},
        "lnkDashView": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[3]"},
        "lnkDashFullScreen": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[4]"},
        "lnkDashProfileIcon": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[5]"},
    },
    "DashWebViewerModule": {
        "btnWevViewerAddIcon": {findBy: "css", value: "button[ng-click='addNewWebPage()']"},
        "txtFldWebViewerSiteUrl": {findBy: "model", value: "webPageInfo.url"},
        "btnWebViewerSiteTest": {findBy: "xpath", value: "//button [contains(text(),'Test')]"},
        "btnWebViewerSiteCancel": {findBy: "css", value: "button[ng-click='close()']"},
        "txtFldWebViewerSiteBookmark": {findBy: "model", value: "webPageInfo.description"},
        "btnWebViewerAddAsLink": {findBy: "css", value: "button[ng-click='addLink()']"},
        "btnWebViewerAddWbSiteAddress": {findBy: "css", value: "button[ng-click='addWebsiteAddress()']"},
        "lnkWebSitesUnderWebViewer": {findBy: "repeater", value: "webSite in webSites"},
        "lnkMediaSitesNavIconUnderWebViewer": {findBy: "xpath", value: "//a[@class='text-muted']/i"},
        "btnSaveWebViewerRecords": {findBy: "xpath", value: "//button [contains(text(),'Save (ctrl+s)')]"}
    },
}

